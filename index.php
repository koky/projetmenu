<?php
if (isset($_GET['date'])) {
  $date = $_GET['date'];
}else{
  $date = new DateTime();
  $date = $date->format('Y-m-d');
}
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Affichage du Menu</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <input type="hidden" name="date" id="date" value="<?php echo $date; ?>">
    <div class="container-fluid">
    <div class="" id="afficheRepas"></div>
  </div>
  </body>
</html>

<script type="text/javascript">


var http = new XMLHttpRequest();
var divParentToPrintContent = document.getElementById('afficheRepas');
let jsonUrl = "php/getmenu.php";
var delParam = [];

window.onload = function(){
  request(jsonUrl,parseJson,"date="+document.getElementById('date').value);
}
// param one : url to call,
// second param : function to call when the data is ready
function request(url1,call,params){
  http.open('POST', url1, true);

  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200 && typeof call === "function") {
          call(http.responseText);
      }
  }

  http.send(params);

}

function parseJson(json){
  let nextId = 0;
  let baseJson = JSON.parse(json);
  let daysOfTheWeek= ["lundi","mardi","mercredi","jeudi","vendredi"];
  let typeRepas = ["dejeuner","diner"];
  let typeProduit = ["entree","plat","accompagnement","laitage","dessert"];
  let typeAttribut = ["bio","maison","local","frais"];
  let containerDate = document.createElement("div");
  containerDate.classList.add("row");
  divParentToPrintContent.appendChild(containerDate);
  let date = document.createElement("h1");
  date.classList.add("col","align-self-center");
  date.appendChild(document.createTextNode("Menu du " + baseJson["date_debut"] + " au " + baseJson["date_fin"]));
  containerDate.appendChild(date);
  let containerHeader = document.createElement("div");
  containerHeader.classList.add("row");
  divParentToPrintContent.appendChild(containerHeader);
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    let colHeader = document.createElement("div");
    if(i == 0){
      colHeader.classList.add("offset-lg-1");
    }
    colHeader.classList.add("col-2","border","border-secondary");
    containerHeader.appendChild(colHeader);
    let dayHeader = document.createElement("h2");
    dayHeader.appendChild(document.createTextNode(daysOfTheWeek[i]));
    colHeader.appendChild(dayHeader);
  }

  for (var i_repas = 0; i_repas < typeRepas.length; i_repas++) {
    let containerRepas = document.createElement("div");
    containerRepas.classList.add("row");
    divParentToPrintContent.appendChild(containerRepas);
    for (var i_day = 0; i_day < daysOfTheWeek.length; i_day++) {
      let colRepas = document.createElement("div");
      if(i_day == 0){
        colRepas.classList.add("offset-lg-1");
      }
      colRepas.classList.add("col-2","border","border-secondary");
      containerRepas.appendChild(colRepas);
      let repasHeader = document.createElement("h3");
      repasHeader.appendChild(document.createTextNode(typeRepas[i_repas]));
      colRepas.appendChild(repasHeader);
      for (var i_typeProduit = 0; i_typeProduit < typeProduit.length; i_typeProduit++) {
        colRepas.appendChild(document.createElement("hr"))

        let containerTypeProduit = document.createElement("div");
        colRepas.appendChild(containerTypeProduit);

        let listProduit = baseJson[daysOfTheWeek[i_day]][typeRepas[i_repas]][typeProduit[i_typeProduit]];

        for (var i_produit = 0; i_produit < listProduit.length; i_produit++) {
          let listAttribut = "";
          for (var i_attribut = 0; i_attribut < typeAttribut.length; i_attribut++) {
            if (listProduit[i_produit][typeAttribut[i_attribut]] == 1) {
              listAttribut += " " + typeAttribut[i_attribut];
            }
          }
          let containerProduit = document.createElement("div");
          containerTypeProduit.appendChild(containerProduit);
          let produitDesignation = document.createElement("p");
          produitDesignation.appendChild(document.createTextNode(listProduit[i_produit]["designation"]));
          containerProduit.appendChild(produitDesignation);
          let produitAttribut = document.createElement("small");
          produitAttribut.appendChild(document.createTextNode(listAttribut));
          produitDesignation.appendChild(produitAttribut);
          let delArea = document.createElement("div");
          containerProduit.appendChild(delArea);
          delArea.innerHTML = "test";
        }
      }
    }
  }
}

</script>
