<?php
include('php/connectBDD.php');
include('php/permission.php');
session_start();
if (hasPermission()) {
  $_POST['error'] = "vous avez été déconnecté";
  session_destroy();
}
if (!empty($_POST['login']) && !empty($_POST['password']) ) {
  $query = "SELECT `login`, `user_permission`.`permission` FROM `user_member` LEFT JOIN `user_permission` ON `user_member`.`login` = `user_permission`.`member` WHERE `login` = ? and `password` = ?";
  $result =  $bdd->prepare($query);
  $result->bindParam(1, $_POST['login'],PDO::PARAM_STR);
  $result->bindParam(2, md5($_POST['password']),PDO::PARAM_STR);
  $result->execute();
  if ($result->rowCount() == 0) {
    $_POST['error'] = "Le mot de passe ou l'identifiant n'est pas le bon";
  }else{
    session_start();
    $_SESSION['permission']['admin'] = 0;
    $_SESSION['permission']['menu'] = 0;
    while ($row = $result->fetch()) {
      if ($row["permission"] == "admin") {
        $_SESSION['permission']['admin'] = 1;
      }elseif ($row["permission"] == "menu") {
        $_SESSION['permission']['menu'] = 1;
      }
    }
    header("Location: admin.php");
  }
}
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Page de Connexion</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-6 offset-3">
          <h1 class="text-center">Connexion</h1>
        </div>
      </div>
      <form class="form-group row" action="login.php" method="post">
        <input class="form-control" type="text" name="login" value="" placeholder="Login" required>
        <input class="form-control" type="password" name="password" value="" placeholder="Password" required>
        <input class="btn btn-primary" type="submit" name="" value="Validation">
        <?php
          if (isset($_POST['error'])) {
            echo $_POST['error'];
          }
         ?>
      </form>
    </div>  
  </body>
</html>
