<?php
  session_start();
  include("php/permission.php");
  if (!hasPermission()) {
    header("Location: login.php");
  }
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="reponse alert alert-info"></div>
      </div>
      <div class="row">
        <a href="login.php">se deconnecter</a>
      </div>
      <div class="row">
        <div class="form-group row">
          <label class="col-3 col-form-label" for="date_select">Semaine</label>
          <div class="col-9">
            <input class="form-control" type="date" name="date_select" value="" id="date_select" onchange="update()" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-9">
          <div id="content"></div>
        </div>
        <form class="col-lg-3">
          <div class="form-group row">
            <label class="col-3 col-form-label" for="designation">Designation</label>
            <div class="col-9">
              <input class="form-control" type="text" name="designation" value="" placeholder="designation" id="designation" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 col-form-label" for="date">Date</label>
            <div class="col-9">
              <input class="form-control" type="date" name="date" value="" id="date" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 col-form-label" for="type_repas">Repas</label>
            <div class="col-9">
              <select class="form-control" name="type_repas" id="type_repas">
                <option value="dejeuner">dejeuner</option>
                <option value="diner">Diner</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-3 col-form-label" for="type_produit">type produit</label>
            <div class="col-9">
              <select class="form-control" name="type_produit" id="type_produit">
                <option value="accompagnement">accompagnement</option>
                <option value="entree">entree</option>
                <option value="plat">plat</option>
                <option value="laitage">laitage</option>
                <option value="dessert">dessert</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-3">Attribut</div>
            <div class="col-9">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="bio" id="bio">
                <label class="form-check-label" for="bio">bio</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="local" id="local">
                <label class="form-check-label" for="local">local</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="maison" id="maison">
                <label class="form-check-label" for="maison">maison</label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="frais" id="frais">
                <label class="form-check-label" for="frais">frais</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col offset-3">
              <button class="btn btn-primary" type="button" name="button" onclick="ajout()" value="">Valider</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </body>
  <script type="text/javascript">
var http = new XMLHttpRequest();
var paramDel = [];
let reponse = document.getElementsByClassName('reponse')[0];

function ajout(){
  let date = "date=" +  document.getElementById('date').value;
  let type_repas = "&type_repas=" + document.getElementById('type_repas').value;
  let designation = "&designation=" + document.getElementById('designation').value;
  let type_produit = "&type_produit=" + document.getElementById('type_produit').value;
  let bio = "&bio=" + Boolean(document.getElementById('bio').checked);
  let local = "&local=" + Boolean(document.getElementById('local').checked);
  let maison = "&maison=" + Boolean(document.getElementById('maison').checked);
  let frais = "&frais=" + Boolean(document.getElementById('frais').checked);
  let params = date + type_repas + designation + type_produit + bio + local + maison + frais;
  alert(params)
  request("php/addproduit.php",printResponse,params,null);
}

function printResponse(text,div){
  text = JSON.parse(text);
  reponse.innerHTML = text["msg"];
  update();
}

function request(url1,call,params,div){
  http.open('POST', url1, true);
  //Send the proper header information along with the request
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

  http.onreadystatechange = function() {//Call a function when the state changes.
      if(http.readyState == 4 && http.status == 200 && typeof call === "function") {
          call(http.responseText,div);
      }
  }
  http.send(params);
}

function parseJson(json,divParentToPrintContent){
  while(divParentToPrintContent.firstChild){
    divParentToPrintContent.removeChild(divParentToPrintContent.firstChild);
  }
  paramDel = [];
  let nextId = 0;
  let baseJson = JSON.parse(json);
  let daysOfTheWeek= ["lundi","mardi","mercredi","jeudi","vendredi"];
  let typeRepas = ["dejeuner","diner"];
  let typeProduit = ["entree","plat","accompagnement","laitage","dessert"];
  let typeAttribut = ["bio","maison","local","frais"];
  let containerDate = document.createElement("div");
  containerDate.classList.add("row");
  divParentToPrintContent.appendChild(containerDate);
  let date = document.createElement("h1");
  date.classList.add("col","offset-1");
  date.appendChild(document.createTextNode("Menu du " + baseJson["date_debut"] + " au " + baseJson["date_fin"]));
  containerDate.appendChild(date);
  let containerHeader = document.createElement("div");
  containerHeader.classList.add("row");
  divParentToPrintContent.appendChild(containerHeader);
  for (var i = 0; i < daysOfTheWeek.length; i++) {
    let colHeader = document.createElement("div");
    if(i == 0){
      colHeader.classList.add("offset-1");
    }
    colHeader.classList.add("col-2","border","border-secondary");
    containerHeader.appendChild(colHeader);
    let dayHeader = document.createElement("h2");
    dayHeader.appendChild(document.createTextNode(daysOfTheWeek[i]));
    colHeader.appendChild(dayHeader);
  }
  for (var i_repas = 0; i_repas < typeRepas.length; i_repas++) {
    let containerRepas = document.createElement("div");
    containerRepas.classList.add("row");
    divParentToPrintContent.appendChild(containerRepas);
    for (var i_day = 0; i_day < daysOfTheWeek.length; i_day++) {
      let colRepas = document.createElement("div");
      if(i_day == 0){
        colRepas.classList.add("offset-lg-1");
      }
      colRepas.classList.add("col-2","border","border-secondary");
      containerRepas.appendChild(colRepas);
      let repasHeader = document.createElement("h3");
      repasHeader.appendChild(document.createTextNode(typeRepas[i_repas]));
      colRepas.appendChild(repasHeader);
      for (var i_typeProduit = 0; i_typeProduit < typeProduit.length; i_typeProduit++) {
        colRepas.appendChild(document.createElement("hr"))

        let containerTypeProduit = document.createElement("div");
        colRepas.appendChild(containerTypeProduit);

        let listProduit = baseJson[daysOfTheWeek[i_day]][typeRepas[i_repas]][typeProduit[i_typeProduit]];

        for (var i_produit = 0; i_produit < listProduit.length; i_produit++) {
          let listAttribut = "";
          for (var i_attribut = 0; i_attribut < typeAttribut.length; i_attribut++) {
            if (listProduit[i_produit][typeAttribut[i_attribut]] == 1) {
              listAttribut += " " + typeAttribut[i_attribut];
            }
          }
          let containerProduit = document.createElement("div");
          containerProduit.classList.add("row");
          containerTypeProduit.appendChild(containerProduit);
          let produitDesignation = document.createElement("p");
          produitDesignation.classList.add("col-9");
          produitDesignation.appendChild(document.createTextNode(listProduit[i_produit]["designation"]));
          containerProduit.appendChild(produitDesignation);
          let produitAttribut = document.createElement("small");
          produitAttribut.appendChild(document.createTextNode(listAttribut));
          produitDesignation.appendChild(produitAttribut);
          let delArea = document.createElement("div");
          delArea.classList.add("col-3");
          containerProduit.appendChild(delArea);
          delArea.innerHTML = "<button type=\"button\" class=\"btn btn-danger\" onclick=\"suppr(" + nextId + ")\">X</button>";
          paramDel.push("date=" + addDays(baseJson["date_debut"], i_day) + "&type_repas=" + typeRepas[i_repas] + "&designation=" + listProduit[i_produit]["designation"] + "&type_produit=" + typeProduit[i_typeProduit]);
          nextId++;
        }
      }
    }
  }
}

function suppr(id){
  request("php/delproduit.php",printResponse,paramDel[id],null);
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result.toISOString();
}

function update(){
  let date_select = document.getElementById('date_select').value;
  let divToApply = document.getElementById('content');
  let param = "date=" + date_select;
  request("php/getmenu.php",parseJson,param,divToApply);
}

function Boolean(a){
  if (a) {
    return 1;
  }
  return 0;
}

update();
</script>
</html>