<?php
	header('Access-Control-Allow-Origin: *');
	$req = "SELECT `menu_repas`.`text_special`, `menu_repas_produit`.*, `menu_produit`.`description` FROM `menu_repas_produit` INNER JOIN `menu_repas` ON `menu_repas_produit`.`date` = `menu_repas`.`date` AND `menu_repas_produit`.`type_repas` = `menu_repas`.`type` INNER JOIN `menu_produit` ON `menu_repas_produit`.`designation` = `menu_produit`.`designation` AND `menu_repas_produit`.`type_produit` = `menu_produit`.`type` WHERE ? <= `menu_repas`.`date` AND `menu_repas`.`date` <= ADDDATE(?, 5) ORDER BY `date` ASC";
	if (isset($_POST["date"]) || is_null($_POST["date"])) {
		$date = new DateTime($_POST["date"]);
	}else{
		$date = new DateTime();
	}
	$date->add(new DateInterval('P2D'));
	$week_start = new DateTime();
	$week_start->setISODate(date("Y", strtotime($date->format('Y-m-d'))),date("W", strtotime($date->format('Y-m-d'))));//lundi de la semaine voulu (semaine du samedi au vendredi)
	try {
		include('connectBDD.php');
		$prepared = $bdd->prepare($req);
		$prepared->execute(array($week_start->format('Y-m-d'), $week_start->format('Y-m-d')));
		$results = $prepared->fetchAll();
	} catch (Exception $e) {
		die($e->getMessage());
	}
	$dayList = array('Mon' => 'lundi', 'Tue' => 'mardi', 'Wed' => 'mercredi', 'Thu' => 'jeudi', 'Fri' => 'vendredi');
	$temp =  array(	'date_debut' => $results[0]["date"],
					'date_fin' => $results[sizeof($results)-1]["date"],	
					'lundi' =>   array(	'dejeuner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													),
									'diner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													)
									),
					'mardi' =>   array(	'dejeuner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													),
									'diner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													)
									),
					'mercredi' =>   array(	'dejeuner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													),
									'diner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													)
									),
					'jeudi' =>   array(	'dejeuner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													),
									'diner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													)
									),
					'vendredi' =>   array(	'dejeuner' =>   array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													),
									'diner' => array(	'entree' => array(),
													'plat' => array(),
													'accompagnement' => array(),
													'laitage' => array(),
													'dessert' => array()
													)
									)
					);
	foreach ($results as $key => $value) {
		array_push($temp[$dayList[date("D", strtotime($value['date']))]][$value["type_repas"]][$value["type_produit"]], array('designation' => $value["designation"],
																					'description' => $value["description"],
																					'bio' => $value['bio'],
																					'maison' => $value['maison'],
																					'local' => $value['local'],
																					'frais' => $value['frais']));
		
	}
	echo json_encode($temp, JSON_UNESCAPED_UNICODE);
?>
